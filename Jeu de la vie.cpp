#include <iostream>

using namespace std;
const int M = 10;
const int N = 10;
void nextGeneration(int grid[M][N], int M, int N)
	{
		int future[N][M]={ { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		};;


		for (int l = 1; l < M - 1; l++)
		{
			for (int m = 1; m < N - 1; m++)
			{

				int voisinsViavants = 0;
				for (int i = -1; i <= 1; i++)
					for (int j = -1; j <= 1; j++)
						voisinsViavants += grid[l + i][m + j];


				voisinsViavants -= grid[l][m];


				if ((grid[l][m] == 1) && (voisinsViavants < 2))
					future[l][m] = 0;


				else if ((grid[l][m] == 1) && (voisinsViavants > 3))
					future[l][m] = 0;


				else if ((grid[l][m] == 0) && (voisinsViavants == 3))
					future[l][m] = 1;

				else
					future[l][m] = grid[l][m];
			}
		}

		cout<<""<<endl;
		for (int i = 0; i < M; i++)
		{
			for (int j = 0; j < N; j++)
			{
				if (future[i][j] == 0)
					cout<<".";
				else
					cout<<"*";
			}
			cout<<""<<endl;
		}


        for (int i = 0; i < M; i++)
		{
			for (int j = 0; j < N; j++)
			{
                grid[i][j]=future[i][j];

			}

		}
	}
int main()
{
    //int M = 10, N = 10;

		// Intiliazing the grid.
		int grid[M][N]  = { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		};
		cout<<"Cr�ation de d�part"<<endl;
			for (int i = 0; i < M; i++)
		{
			for (int j = 0; j < N; j++)
			{
				if (grid[i][j] == 0)
					cout<<".";
				else
					cout<<"*";
			}
			cout<<""<< endl;
		}
		cout<<""<< endl;
		bool continuer=true;
		while(continuer)
        {
            int c;
            cout<<"Tapez 0 pour une nouvelle it�ration et 1 pour arreter"<< endl;
            cin>>c;
            if(c==0)
            {
                continuer==true;
                nextGeneration(grid, M, N);
            }
            else
                continuer==false;

        }


    return 0;
}
